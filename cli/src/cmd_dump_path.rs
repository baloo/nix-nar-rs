use clap::Parser;
use nix_nar::Encoder;
use std::{
    io::{self, Write},
    path::PathBuf,
};

#[derive(Parser)]
pub struct Opts {
    /// The path to serialize, which must contain only regular files,
    /// directories and symbolic links.
    pub path: PathBuf,
}

pub fn run<W: Write>(mut w: W, Opts { path }: Opts) -> Result<(), anyhow::Error> {
    let mut enc = Encoder::new(path);
    io::copy(&mut enc, &mut w)?;
    Ok(())
}
