.PHONY: all release test check

all:
	cargo build --workspace

release:
	cargo build --release --workspace

test:
	cargo build --tests --workspace
	timeout 60 cargo test --workspace -- --nocapture

clippy:
	cargo clippy --workspace

check:
	cargo check --workspace
