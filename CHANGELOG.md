0.2.0
=====

This release hides the modules, re-exports the data structures, and
improves docs all around.

### BREAKING CHANGES:
  - Reworked module hierarchy such that modules are not public, but
    their structs are public.

### New features:
  - Add `Encoder::pack`,
  - Add `Decoder::unpack`,
  - Add `EncoderBuilder` to set encoder options,
  - Add `debug::pretty_print_nar_content`.
  
### Changes:
  - Remove the 128 byte requirement on `Encoder::read`'s buffer.  The
    requirement is now on `Encoder`'s internal buffer, which defaults
    to 1024 bytes.
