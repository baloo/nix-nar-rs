#![cfg_attr(debug_assertions, allow(dead_code, unused_imports, unused_variables))]

//! # Summary
//!
//! Library to manipulate Nix Archive (nar) files.
//!
//! For the NAR specification, see ["The Purely Functional Deployment
//! Model" by Eelco
//! Dolstra](https://edolstra.github.io/pubs/phd-thesis.pdf), Figure
//! 5.2 (page 93).
//!
//! ## Encoding
//!
//! To encode a directory as a NAR file, first create an [`Encoder`]
//! with [`Encoder::new`], then treat it as a [`std::io::Read`]
//! instance.  For instance, you can [`std::io::copy`] it to a file.
//!
//! ```no_run
//! use nix_nar::Encoder;
//! # use std::{io, fs::File};
//! # fn main() -> io::Result<()> {
//! let mut enc = Encoder::new("some/dir");
//! let mut nar = File::create("output.nar")?;
//! io::copy(&mut enc, &mut nar)?;
//! # Ok(())
//! # }
//! ```
//!
//! If you have very long filenames, use [`Encoder::builder()`] and
//! then configure the internal buffer size to be at least 200 bytes
//! larger than the longest filename.
//!
//! Alternatively, use the [`Encoder::pack`] helper.
//!
//! ## Decoding
//!
//! To decode a NAR file, first create a [`Decoder`] with
//! [`Decoder::new`], and then call [`Decoder::entries`] to iterate
//! through the files in the archive.
//!
//! ```
//!   use nix_nar::Decoder;
//!   # use nix_nar::NarError;
//!   # fn main() -> Result<(), NarError> {
//!   let input = include_bytes!("../test-data/02-empty-file.nar");
//!   let dec = Decoder::new(&input[..])?;
//!   for entry in dec.entries()? {
//!     let entry = entry?;
//!     println!("{:?} {:?}", entry.path, entry.content);
//!   }
//!   # Ok(())
//!   # }
//! ```
//!
//! Alternatively, use [`Decoder::unpack`] to extract a NAR file to a
//! directory.

mod coder;
pub mod debug;
mod dec;
mod enc;
mod error;
mod parser;

pub use crate::dec::{Content, Decoder, Entries, Entry};
pub use crate::enc::{Encoder, EncoderBuilder};
pub use crate::error::NarError;
