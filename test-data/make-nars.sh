#!/bin/sh

set -e -x

for f in *.in; do
    nix nar dump-path "$f" > "${f%.in}.nar"
done
