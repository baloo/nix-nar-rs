nix-nar-rs
==========

> Library and binary to manipulate Nix Archive (nar) files

## Library usage

See the [docs](https://docs.rs/nix-nar/latest/nix_nar/) for details.

### Encoding

To encode a directory as a NAR file, first create an `Encoder`
with `Encoder::new`, then treat it as a `std::io::Read`
instance.  For instance, you can `std::io::copy` it to a file.

```rust
use nix_nar::Encoder;
let mut enc = Encoder::new("some/dir");
let mut nar = File::create("output.nar")?;
io::copy(&mut enc, &mut nar)?;
```

### Decoding

To decode a NAR file, first create a `Decoder` with `Decoder::new`,
and then call `Decoder::entries` to iterate through the files in the
archive.

```rust
use nix_nar::Decoder;
let input = include_bytes!("../test-data/02-empty-file.nar");
let dec = Decoder::new(&input[..])?;
for entry in dec.entries()? {
  let entry = entry?;
  println!("{:?} {:?}", entry.path, entry.content);
}
```

## Command line usage

The `nix-nar` command line tool tries to match [`nix nar`][nix-nar] as
much as possible.  It has the same subcommands, options, and output.

[nix-nar]: https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-nar.html

There are a few differences in the wording of the error messages, and
the `--directory` flag for the `ls` subcommand is not supported
(because I don't think it has reasonable semantics in `nix nar`).

```
USAGE:
    nix-nar <SUBCOMMAND>

OPTIONS:
    -h, --help       Print help information
    -V, --version    Print version information

SUBCOMMANDS:
    cat          Print the contents of a file inside a NAR file on stdout
    dump-path    Serialise a path to stdout in NAR format
    help         Print this message or the help of the given subcommand(s)
    ls           Show information about a path inside a NAR file
```


## Installation

### Library

Add the following to your `Cargo.toml`:

```toml
nix-nar = "0.2"
```

### Binary

You can install the binary either through Cargo, or through the nix
flake:

```shell-session
$ cargo install nix-nar-cli
```

```shell-session
$ nix shell gitlab:abstract-binary/nix-nar-rs       # temporarily
$ nix profile add gitlab:abstract-binary/nix-nar-rs # permanently
```


## References

- ["The Purely Functional Deployment Model" by Eelco Dolstra][phd-thesis], Figure 5.2 (page 93)
- https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-nar.html

[phd-thesis]: https://edolstra.github.io/pubs/phd-thesis.pdf


## License

This software is dual-licensed under Apache-2.0 and LGPL-2.1-or-later.

### Apache 2.0

```
Copyright 2022 Alexandru Scvortov

Licensed under the Apache License, Version 2.0 (the "License"); you
may not use this file except in compliance with the License.  You may
obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.  See the License for the specific language governing
permissions and limitations under the License.
```

### GNU Lesser General Public License 2.1 or later

```
Copyright (C) 2022 Alexandru Scvortov

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option)
any later version.

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.
```
